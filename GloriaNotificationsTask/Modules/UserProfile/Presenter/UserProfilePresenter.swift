
import UIKit

protocol UserProfilePresentable: UIViewController {
    
    func showProfile(_ profile: UserProfileData)
    func showError(_ error: Error)
}

class UserProfilePresenter {
    
    weak var view: UserProfilePresentable?
    
    private let getProfileInteractor = GetUserProfileInteractor()
    
    private let router: UserProfileRouter
   
    private let userId: String
    
    init(userId: String,
         router: UserProfileRouter) {
        self.userId = userId
        self.router = router
        getProfileInteractor.delegate = self
    }
    
    func getProfile() {
        
        getProfileInteractor.execute(userId: userId)
    }
}

extension UserProfilePresenter: GetUserProfileInteractorDelegate {
    
    func getUserProfileDidResponse(profile: UserProfileData) {
        
        view?.showProfile(profile)
    }
    
    func getUserProfileDidFail(error: Error) {
        
        view?.showError(error)
    }
}
