
import UIKit

class UserProfileView: UIView {
    
    @IBOutlet var avatarImageView: UIImageView?
    
    @IBOutlet var videosCountLabel: RegularLabel?
    @IBOutlet var followersCountLabel: RegularLabel?
    @IBOutlet var followingCountLabel: RegularLabel?

    @IBOutlet var collection: UserProfileVideosCollection?
    
    private let avatarPlaceholderImage = UIImage(named: "user-avatar-placeholder")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup(data: UserProfileData?) {
                
        if let imageId = data?.profile?.avatarImageId {
            avatarImageView?.image = UIImage(named: imageId) ?? avatarPlaceholderImage
        }
        
        videosCountLabel?.text = String(format: "n_videos".localized(),
                                        data?.videos.count ?? 0)
        
        followersCountLabel?.text = String(format: "n_followers".localized(),
                                           data?.profile?.followersCount ?? 0)
        
        followingCountLabel?.text = String(format: "n_following".localized(),
                                           data?.profile?.followingCount ?? 0)
        
        collection?.videos = data?.videos ?? []
    }
    
    private func setup() {
        
        avatarImageView?.layer.cornerRadius = 64
        avatarImageView?.layer.masksToBounds = true
        
        clear()
    }
    
    private func clear() {
        
        setup(data: nil)
        avatarImageView?.image = avatarPlaceholderImage
    }
}
