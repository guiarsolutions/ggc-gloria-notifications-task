
import UIKit

class UserProfileVideoCollectionCell: UICollectionViewCell {

    static let reuseIdentifier = "UserProfileVideoCollectionCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        backgroundColor = Colors.grayColor
    }
}
