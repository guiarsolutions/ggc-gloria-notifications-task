
import UIKit

class UserProfileViewController: UIViewController {
    
    private let mainView = UserProfileView.loadFromNib()
    
    private let presenter: UserProfilePresenter
    
    init(presenter: UserProfilePresenter) {
        self.presenter = presenter
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.getProfile()
    }
    
    private func setup() {
        
        presenter.view = self
    }
}

extension UserProfileViewController : UserProfilePresentable {
    
    func showProfile(_ profile: UserProfileData) {
        
        title = profile.user.username.localizedUppercase
        mainView.setup(data: profile)
    }
    
    func showError(_ error: Error) {}
}
