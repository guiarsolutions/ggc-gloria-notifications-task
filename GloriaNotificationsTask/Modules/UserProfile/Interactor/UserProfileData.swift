
import Foundation

struct UserProfileData {
    
    let user: User
    let profile: UserProfile?
    let videos: [UserVideo]
}
