
import RxSwift

protocol GetUserProfileInteractorDelegate : class {
    
    func getUserProfileDidResponse(profile: UserProfileData)
    func getUserProfileDidFail(error: Error)
}

class GetUserProfileInteractor {
    
    weak var delegate: GetUserProfileInteractorDelegate?
    
    private let repository = UserRepository()
    
    private var disposable: Disposable?
    
    func execute(userId: String) {
        
        disposable?.dispose()
        
        disposable = Observable.zip(repository.getUser(id: userId),
                                    repository.getProfile(userId: userId),
                                    repository.getVideos(userId: userId))
            .subscribe(onNext: { [weak self] (user, profile, videos) in
                
                guard let user = user else {
                    
                    self?.delegate?.getUserProfileDidFail(error: GetUserProfileError(userId: userId))
                    return
                }
                
                let data = UserProfileData(user: user,
                                           profile: profile,
                                           videos: videos)
                
                self?.delegate?.getUserProfileDidResponse(profile: data)
                
                }, onError: { [weak self] (error) in
                    
                    self?.delegate?.getUserProfileDidFail(error: error)
                    
                }, onCompleted: nil, onDisposed: nil)
    }
}

class GetUserProfileError: LocalizedError {
    
    private let userId: String
    
    public var localizedDescription: String { return "Not exists profile for user id = \(userId)" }
    
    init(userId: String) {
        self.userId = userId
    }
}
