
import UIKit

class UserProfileRouter {
    
    static func start(userId: String) -> UIViewController {
        
        let presenter = UserProfilePresenter(userId: userId,
                                             router: UserProfileRouter())
        
        return UserProfileViewController(presenter: presenter)
    }
}
