
import UIKit

class SettingsViewController: UIViewController {

    init() {
        super.init(nibName: "SettingsViewController", bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
