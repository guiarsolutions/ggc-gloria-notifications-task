
import UIKit

class HomeViewController: UIViewController {

    @IBOutlet var notificationsButton: PrimaryButton?
    
    init() {
        super.init(nibName: "HomeViewController", bundle: Bundle.main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        
        notificationsButton?.setTitle("show_notifications_button".localized(),
                                      for: .normal)
        
        notificationsButton?.addTarget(self,
                                       action: #selector(showNotifications),
                                       for: .touchUpInside)
    }
    
    @objc private func showNotifications() {
       
        NotificationsRouter.start(from: self)
    }
}
