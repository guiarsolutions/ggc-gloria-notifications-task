
import UIKit

class TabController: UITabBarController {
    
    private lazy var home = createController(HomeViewController(),
                                             title: "home_tab_title".localized(),
                                             unselectedImage: UIImage(named: "tab-icon-home"),
                                             selectedImage: UIImage(named: "tab-icon-home-filled"))
    
    private lazy var settings = createController(SettingsViewController(),
                                                 title: "settings_tab_title".localized(),
                                                 unselectedImage: UIImage(named: "tab-icon-settings"),
                                                 selectedImage: UIImage(named: "tab-icon-settings-filled"))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        setupTabBar()
        setupControllers()
    }
    
    private func setupTabBar() {
        tabBar.barStyle = .blackOpaque
        tabBar.isTranslucent = false
        tabBar.tintColor = Colors.tabBarSelectedItemColor
        tabBar.barTintColor = Colors.tabBarBackgroundColor
        tabBar.unselectedItemTintColor = Colors.tabBarUnselectedItemColor
    }
    
    private func setupControllers() {
        viewControllers = [home, settings]
    }
}

extension TabController {
    
    private func createController(_ controller: UIViewController,
                               title: String?,
                               unselectedImage: UIImage?,
                               selectedImage: UIImage?) -> UIViewController {
        
        let nav = NavigationController(rootViewController: controller)
        controller.title = title
        nav.tabBarItem.image = unselectedImage
        nav.tabBarItem.selectedImage = selectedImage
        return nav
    }
}
