import UIKit
import MessageUI

open class NavigationController: UINavigationController {
    
    private var backButton: UIBarButtonItem {
        
        return UIBarButtonItem(image: UIImage(named: "navigation-back-arrow"),
                               style: .plain,
                               target: self,
                               action: #selector(goBack))
    }
    
    public convenience init(rootViewController: UIViewController,
                            modalPresentationStyle: UIModalPresentationStyle) {
        self.init(rootViewController: rootViewController)
        self.modalPresentationStyle = modalPresentationStyle
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        styleNavBar()
        styleStatusBar()
        modalPresentationStyle = .fullScreen
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        if viewControllers.count > 0 {
            viewController.navigationItem.leftBarButtonItem = backButton
        }
        
        super.pushViewController(viewController, animated: animated)
    }
    
    @objc private func goBack() {
        _ = popViewController(animated: true)
    }
    
    private func styleNavBar() {

        navigationBar.isTranslucent = false

        let font = Fonts.semibold(size: 24)
        let fontColor = Colors.blackColor
        
        let barAppearance = UINavigationBar.appearance()
        barAppearance.barStyle = .default
        barAppearance.tintColor = Colors.navigationBarTint
        barAppearance.barTintColor = Colors.navigationBarBackgroundColor
        barAppearance.titleTextAttributes = [.font: font,
                                             .foregroundColor: fontColor]
        
        navigationBar.layer.shadowColor = Colors.navigationBarShadowColor
        navigationBar.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        navigationBar.layer.shadowRadius = 0.5
        navigationBar.layer.shadowOpacity = 0.7
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
    
    private func styleStatusBar() {
        
        var statusBarView: UIView?
        
        if #available(iOS 13.0, *) {
            let tag = 99999999
            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                statusBarView = statusBar
            } else {
                let statusBar = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBar.tag = tag
                UIApplication.shared.keyWindow?.addSubview(statusBar)
                statusBarView = statusBar
            }
        } else {
            statusBarView = UIApplication.shared.value(forKey: "statusBar") as? UIView
        }
           
        statusBarView?.backgroundColor = Colors.navigationBarBackgroundColor
    }
}
