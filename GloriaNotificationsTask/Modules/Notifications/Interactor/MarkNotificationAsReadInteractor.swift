
import RxSwift

protocol MarkNotificationAsReadInteractorDelegate : class {
    
    func markNotificationAsReadDidResponse(success: Bool)
    func markNotificationAsReadDidFail(error: Error)
}

class MarkNotificationAsReadInteractor {
    
    weak var delegate: MarkNotificationAsReadInteractorDelegate?
    
    private let repository = NotificationsRepository()
    
    private var disposable: Disposable?
    
    func execute(id: String) {
        
        disposable?.dispose()
        
        disposable = repository.markAsRead(notificationId: id).subscribe(onNext: { [weak self] (success) in
            
            self?.delegate?.markNotificationAsReadDidResponse(success: success)
            
            }, onError: { [weak self] (error) in
                
                self?.delegate?.markNotificationAsReadDidFail(error: error)
                
            }, onCompleted: nil, onDisposed: nil)
    }
}
