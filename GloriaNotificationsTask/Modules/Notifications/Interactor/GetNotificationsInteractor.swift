
import RxSwift

protocol GetNotificationsInteractorDelegate : class {
    
    func getNotificationsDidResponse(notifications: [Notification])
    func getNotificationsDidFail(error: Error)
}

class GetNotificationsInteractor {
    
    weak var delegate: GetNotificationsInteractorDelegate?
    
    private let repository = NotificationsRepository()
    
    private var disposable: Disposable?
    
    func execute() {
        
        disposable?.dispose()
        
        disposable = repository.getNotifications().subscribe(onNext: { [weak self] (notifications) in
            
            self?.delegate?.getNotificationsDidResponse(notifications: notifications)
            
            }, onError: { [weak self] (error) in
                
                self?.delegate?.getNotificationsDidFail(error: error)
                
            }, onCompleted: nil, onDisposed: nil)
    }
}
