
import UIKit

class NotificationsView: UIView {
    
    @IBOutlet var table: UITableView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    private func setup() {
        
        table?.separatorStyle = .none
    }
}
