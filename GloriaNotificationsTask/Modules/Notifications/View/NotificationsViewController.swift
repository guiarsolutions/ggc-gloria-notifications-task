
import UIKit

protocol NotificationCell: UITableViewCell {
    
    func setup(notification: Notification)
}

class NotificationsViewController: UIViewController {
    
    private let mainView = NotificationsView.loadFromNib()
    
    private let presenter: NotificationsPresenter
    
    private var notifications = [Notification]()
    
    init(presenter: NotificationsPresenter) {
        self.presenter = presenter
        super.init(nibName: .none, bundle: .none)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.getNotifications()
    }
    
    private func setup() {
        title = "NOTIFICATIONS"
        setupTable()
        presenter.view = self
        navigationItem.rightBarButtonItem = NavigationItems.close(target: self,
                                                                  action: #selector(close))
    }
    
    private func setupTable() {
        
        guard let table = mainView.table else {
            return
        }
        
        table.register(VideoNotificationCell.nib(),
                       forCellReuseIdentifier: VideoNotificationCell.reuseIdentifier)
        table.register(UserNotificationCell.nib(),
                       forCellReuseIdentifier: UserNotificationCell.reuseIdentifier)
        table.delegate = self
        table.dataSource = self
    }
    
    private func cellIdentifierForType(_ type: NotificationType) -> String {
        
        switch type {
            case .user:
                return UserNotificationCell.reuseIdentifier
            case .video:
                return VideoNotificationCell.reuseIdentifier
            default:
                return ""
        }
    }
    
    @objc private func close() {
        
        dismiss(animated: true, completion: nil)
    }
}

extension NotificationsViewController : NotificationsPresentable {
    
    func showNotifications(_ notifications: [Notification]) {
        
        self.notifications = notifications
        mainView.table?.reloadData()
    }
    
    func showError(_ error: Error) {}
}

extension NotificationsViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 64
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let notification = notifications[indexPath.row]
        
        let identifier = cellIdentifierForType(notification.type)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier,
                                                 for: indexPath)
        
        if let notifCell = cell as? NotificationCell {
            
            notifCell.setup(notification: notification)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        presenter.pushNotification(notifications[indexPath.row])
    }
}
