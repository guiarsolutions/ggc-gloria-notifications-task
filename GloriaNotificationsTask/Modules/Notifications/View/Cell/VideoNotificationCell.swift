
import UIKit

class VideoNotificationCell: UITableViewCell, NotificationCell {

    @IBOutlet var unreadIndicator: UnreadNotificationIndicator?
    @IBOutlet var messageLabel: FormattedLabel?
    @IBOutlet var videoThumbnailImageView: UIImageView?
    
    static let reuseIdentifier = "VideoNotificationCell"
        
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func setup(notification: Notification) {
        
        unreadIndicator?.isHidden = !notification.unread
        messageLabel?.formattedText = notification.formattedMessage
        videoThumbnailImageView?.image = UIImage(named: notification.imageIdentifier)
    }
    
    private func setup() {
        
        prepareForReuse()
        
        videoThumbnailImageView?.backgroundColor = Colors.videoPlaceBackgroundColor
        videoThumbnailImageView?.layer.cornerRadius = 5
        videoThumbnailImageView?.layer.masksToBounds = true
    }
    
    private func clear() {
        
        unreadIndicator?.isHidden = true
        messageLabel?.formattedText = ""
        videoThumbnailImageView?.image = nil
    }
}
