
import UIKit

class UserNotificationCell: UITableViewCell, NotificationCell {

    @IBOutlet var unreadIndicator: UnreadNotificationIndicator?
    @IBOutlet var messageLabel: FormattedLabel?
    @IBOutlet var avatarImageView: UIImageView?
    
    static let reuseIdentifier = "UserNotificationCell"
    
    private let avatarPlaceholderImage = UIImage(named: "user-avatar-placeholder")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }
    
    func setup(notification: Notification) {
        
        unreadIndicator?.isHidden = !notification.unread
        messageLabel?.formattedText = notification.formattedMessage
        avatarImageView?.image = UIImage(named: notification.imageIdentifier) ?? avatarPlaceholderImage
    }
    
    private func setup() {
        
        prepareForReuse()
        
        avatarImageView?.layer.cornerRadius = 12
        avatarImageView?.layer.masksToBounds = true
    }
    
    private func clear() {
        
        unreadIndicator?.isHidden = true
        messageLabel?.formattedText = ""
        avatarImageView?.image = avatarPlaceholderImage
    }
}
