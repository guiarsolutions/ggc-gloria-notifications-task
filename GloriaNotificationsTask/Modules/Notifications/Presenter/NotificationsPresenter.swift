
import UIKit

protocol NotificationsPresentable: UIViewController {
    
    func showNotifications(_ notifications: [Notification])
    func showError(_ error: Error)
}

class NotificationsPresenter {
    
    weak var view: NotificationsPresentable?
    
    private let getNotificationsInteractor = GetNotificationsInteractor()
    private let markNotificationAsReadInteractor = MarkNotificationAsReadInteractor()
    
    private let router: NotificationsRouter
    
    init(router: NotificationsRouter) {
        self.router = router
        getNotificationsInteractor.delegate = self
        markNotificationAsReadInteractor.delegate = self
    }
    
    func getNotifications() {
        
        getNotificationsInteractor.execute()
    }
    
    func pushNotification(_ notification: Notification) {
        
        guard let navigation = view?.navigationController else {
            return
        }
        
        if router.pushNotification(notification, from: navigation) {
            
            markAsRead(notification: notification)
        }
    }
    
    private func markAsRead(notification: Notification) {
        
        guard notification.unread else {
            return
        }
        
        markNotificationAsReadInteractor.execute(id: notification.id)
    }
}

extension NotificationsPresenter: GetNotificationsInteractorDelegate {
    
    func getNotificationsDidResponse(notifications: [Notification]) {
        
        view?.showNotifications(notifications)
    }
    
    func getNotificationsDidFail(error: Error) {
        
        view?.showError(error)
    }
}

extension NotificationsPresenter: MarkNotificationAsReadInteractorDelegate {
    
    func markNotificationAsReadDidResponse(success: Bool) {
        
        if success {
            getNotifications()
        }
    }
    
    func markNotificationAsReadDidFail(error: Error) {}
}
