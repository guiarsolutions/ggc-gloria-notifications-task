
import UIKit

class NotificationsRouter {
    
    static func start(from view: UIViewController) {
        
        let presenter = NotificationsPresenter(router: NotificationsRouter())
        
        let viewController = NotificationsViewController(presenter: presenter)
        
        let navigation = NavigationController(rootViewController: viewController)
        navigation.modalPresentationStyle = .fullScreen
        navigation.modalTransitionStyle = .coverVertical
        
        view.present(navigation, animated: true)
    }
    
    func pushNotification(_ notification: Notification,
                          from navigation: UINavigationController) -> Bool {
        
        guard let viewController = viewControllerFor(navigation: notification.navigation) else {
            return false
        }
        
        navigation.pushViewController(viewController, animated: true)
        
        return true
    }
    
    private func viewControllerFor(navigation: NotificationNavigation) -> UIViewController? {
        
        switch navigation.type {
        case .profile:
            guard let userId = navigation.entityId else {
                return nil
            }
            return UserProfileRouter.start(userId: userId)
        default:
            return nil
        }
    }
}
