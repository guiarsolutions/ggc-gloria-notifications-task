
import Foundation

struct UserProfile {
    
    let userId: String
    let avatarImageId: String
    let followersCount: Int
    let followingCount: Int
}
