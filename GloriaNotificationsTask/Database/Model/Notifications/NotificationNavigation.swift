
enum NotificationNavigationType {
    
    case profile
    case none
}

struct NotificationNavigation {
    
    let type: NotificationNavigationType
    let entityId: String?

    init(type: NotificationNavigationType,
         entityId: String? = nil) {
    
        self.type = type
        self.entityId = entityId
    }
}
