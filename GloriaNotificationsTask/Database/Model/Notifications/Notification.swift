
import Foundation

struct Notification {
    
    let id: String
    let type: NotificationType
    let message: String
    let formattedMessage: String
    let imageIdentifier: String
    let navigation: NotificationNavigation
    private(set) var unread: Bool
    
    mutating func markAsRead() {
        unread = false
    }
}
