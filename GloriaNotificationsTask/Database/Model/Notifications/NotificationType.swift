
import Foundation

enum NotificationType: String {
    
    case user = "user"
    case video = "video"
    case unknown = ""
}
