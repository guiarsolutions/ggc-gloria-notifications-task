
import Foundation

class Database {
    
    static let shared = Database()
    
    private lazy var notifications: [Notification] = {
       
        let notifications = [Notification(id: "1",
                                          type: .video,
                                          message: "Matteo liked your video",
                                          formattedMessage: "<b>Matteo</b> liked your video",
                                          imageIdentifier: "image1",
                                          navigation: NotificationNavigation(type: .none),
                                          unread: true),
                             Notification(id: "2",
                                          type: .user,
                                          message: "matias is now following you",
                                          formattedMessage: "<b>matias</b> is now following you",
                                          imageIdentifier: "image2",
                                          navigation: NotificationNavigation(type: .profile,
                                                                             entityId: "1"),
                                          unread: true),
                             Notification(id: "3",
                                          type: .video,
                                          message: "There's a new challenge for you to check out",
                                          formattedMessage: "There's a new challenge for you to check out",
                                          imageIdentifier: "image3",
                                          navigation: NotificationNavigation(type: .none),
                                          unread: true),
                             Notification(id: "4",
                                          type: .video,
                                          message: "New Toilet Paper Challenge. Check it out!",
                                          formattedMessage: "New <b>Toilet Paper Challenge</b>. Check it out!",
                                          imageIdentifier: "image4",
                                          navigation: NotificationNavigation(type: .none),
                                          unread: false),
                             Notification(id: "5",
                                          type: .user,
                                          message: "Check out Anton's profile",
                                          formattedMessage: "Check out <b>Anton</b>'s profile",
                                          imageIdentifier: "",
                                          navigation: NotificationNavigation(type: .none),
                                          unread: false),
                             Notification(id: "6",
                                          type: .video,
                                          message: "Here is a new video we like",
                                          formattedMessage: "Here is a <b>new video</b> we like",
                                          imageIdentifier: "image6",
                                          navigation: NotificationNavigation(type: .none),
                                          unread: false),
                             Notification(id: "7",
                                          type: .user,
                                          message: "Here is a profile we like",
                                          formattedMessage: "Here is a <b>profile</b> we like",
                                          imageIdentifier: "image7",
                                          navigation: NotificationNavigation(type: .none),
                                          unread: false)]
        
        return notifications
    }()
    
    private lazy var users: [User] = {
       
        return [User(id: "1", username: "Matias")]
    }()
    
    private lazy var profiles: [UserProfile] = {
       
        return [UserProfile(userId: "1",
                            avatarImageId: "image2",
                            followersCount: 123,
                            followingCount: 45)]
    }()
    
    private lazy var videos: [UserVideo] = {
       
        return [UserVideo(id: "1", userId: "1"),
                UserVideo(id: "2", userId: "1"),
                UserVideo(id: "3", userId: "1"),
                UserVideo(id: "4", userId: "1")]
    }()
    
    private init() {}
    
    static func getUser(id: String) -> User? {
        
        return shared.users.first(where: { $0.id == id })
    }
    
    static func getProfile(userId: String) -> UserProfile? {
        
        return shared.profiles.first(where: { $0.userId == userId })
    }
    
    static func getVideos(userId: String) -> [UserVideo] {
        
        return shared.videos.filter({ $0.userId == userId })
    }
    
    static func getNotifications() -> [Notification] {
        
        return shared.notifications
    }
    
    static func getNotification(id: String) -> Notification? {
        
        return shared.notifications.first(where: { $0.id == id })
    }
    
    static func updateNotification(notification: Notification) -> Bool {
        
        guard let index = shared.notifications.firstIndex(where: { $0.id == notification.id }) else {
            return false
        }
        
        shared.notifications[index] = notification
        
        return true
    }
}
