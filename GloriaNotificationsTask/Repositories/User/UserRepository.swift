
import RxSwift

class UserRepository {
    
    func getUser(id: String) -> Observable<User?> {
        
        return Observable<User?>.just(Database.getUser(id: id))
    }
    
    func getProfile(userId: String) -> Observable<UserProfile?> {
        
        return Observable<UserProfile?>.just(Database.getProfile(userId: userId))
    }
    
    func getVideos(userId: String) -> Observable<[UserVideo]> {
        
        return Observable<[UserVideo]>.just(Database.getVideos(userId: userId))
    }
}
