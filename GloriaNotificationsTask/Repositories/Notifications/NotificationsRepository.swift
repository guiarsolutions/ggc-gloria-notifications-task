
import RxSwift

class NotificationsRepository {
    
    func getNotifications() -> Observable<[Notification]> {
        
        return Observable<[Notification]>.just(Database.getNotifications())
    }
    
    func markAsRead(notificationId: String) -> Observable<Bool> {
        
        guard var notification = Database.getNotification(id: notificationId) else {
            return Observable<Bool>.just(false)
        }
        
        notification.markAsRead()
        
        return Observable<Bool>.just(Database.updateNotification(notification: notification))
    }
}
