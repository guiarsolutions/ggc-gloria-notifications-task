
import Foundation
import UIKit

class AttributedStringBuilder {
    
    private let boldOpenTag = "<b>"
    private let boldCloseTag = "</b>"
    
    private let textColor: UIColor
    private let regularFont: UIFont
    private let boldFont: UIFont
    
    private lazy var regularAttributes: [NSMutableAttributedString.Key: Any] = [.font: self.regularFont,
                                                                                .foregroundColor: self.textColor]
    
    private lazy var boldAttributes: [NSMutableAttributedString.Key: Any] = [.font: self.boldFont,
                                                                             .foregroundColor: self.textColor]
    
    init(textColor: UIColor,
         regularFont: UIFont,
         boldFont: UIFont) {
        
        self.textColor = textColor
        self.regularFont = regularFont
        self.boldFont = boldFont
    }
    
    func buildWith(formattedText: String) -> NSAttributedString {
        
        var nsString: NSString = formattedText as NSString
        
        let boldRanges = substractBoldRanges(string: &nsString)
        
        let attributedString = NSMutableAttributedString(string: nsString as String,
                                                         attributes: regularAttributes)
        
        for range in boldRanges {
            attributedString.addAttributes(boldAttributes, range: range)
        }
        
        return attributedString
    }
}

extension AttributedStringBuilder {
    
    private func substractBoldRanges(string: inout NSString) -> [NSRange] {
        
        var boldRanges = [NSRange]()
        
        while string.contains(boldOpenTag) {
            
            let openRange = string.range(of: boldOpenTag)
            
            var closeRange = string.range(of: boldCloseTag)
            closeRange.location -= boldOpenTag.count
            
            let boldStringRange = NSRange(location: openRange.location,
                                          length: closeRange.location-openRange.location)
            boldRanges.append(boldStringRange)
            
            string = string.replacingCharacters(in: openRange, with: "") as NSString
            string = string.replacingCharacters(in: closeRange, with: "") as NSString
        }
        
        return boldRanges
    }
}
