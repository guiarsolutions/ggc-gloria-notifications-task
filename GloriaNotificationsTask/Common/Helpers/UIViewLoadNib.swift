import UIKit

public protocol LoadNib {}
extension UIView : LoadNib {}

public extension LoadNib where Self: UIView {
    
    static func loadFromNib(bundle: Bundle = Bundle.main) -> Self {
        return nib(bundle: bundle).instantiate(withOwner: self, options: nil).first as! Self
    }
    
    static func nib(bundle: Bundle = Bundle.main) -> UINib {
        let nibName = "\(self)".split{$0 == "."}.map(String.init).last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib
    }
}
