
import UIKit

class Fonts {
    
    static let regular = "OpenSans-Regular"
    static let semibold = "OpenSans-Semibold"
    static let bold = "OpenSans-Bold"
    
    static func regular(size: CGFloat) -> UIFont {
        
        return UIFont(name: regular, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func semibold(size: CGFloat) -> UIFont {
        
        return UIFont(name: semibold, size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }
    
    static func bold(size: CGFloat) -> UIFont {
        
        return UIFont(name: bold, size: size) ?? UIFont.boldSystemFont(ofSize: size)
    }
}
