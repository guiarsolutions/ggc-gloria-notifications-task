
import UIKit

@IBDesignable open class FormattedLabel: RegularLabel {
   
    private let builder = AttributedStringBuilder(textColor: Colors.blackColor,
                                                  regularFont: Fonts.regular(size: 15),
                                                  boldFont: Fonts.bold(size: 15))
    
    @IBInspectable var formattedText: String? {
        didSet {
            setup()
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    open override func setup() {
        super.setup()
        format()
    }

    private func format() {
        guard let formattedText = formattedText else {
            attributedText = NSAttributedString(string: text ?? "")
            return
        }
        attributedText = builder.buildWith(formattedText: formattedText)
    }
}
