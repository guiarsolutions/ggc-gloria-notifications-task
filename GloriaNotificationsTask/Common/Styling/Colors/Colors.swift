
import UIKit

class Colors {
    
    static let blackColor = UIColor(hexString: "000000")
    static let grayColor = UIColor(hexString: "E7E7E7")
    static let whiteColor = UIColor(hexString: "FFFFFF")
    static let primaryDarkColor = UIColor(hexString: "0077C2")
    static let primaryLightColor = UIColor(hexString: "80D6FF")
    
    static let tabBarBackgroundColor = primaryDarkColor
    static let tabBarSelectedItemColor = whiteColor
    static let tabBarUnselectedItemColor = primaryLightColor
    
    static let navigationBarTint = blackColor
    static let navigationBarBackgroundColor = whiteColor
    static let navigationBarShadowColor = blackColor.withAlphaComponent(0.5).cgColor
    
    static let primaryButtonBackgroundColor = primaryDarkColor
    static let primaryButtonTitleColor = whiteColor
    
    static let unreadNotificationIndicatorColor = UIColor(hexString: "FA5050")
    
    static let videoPlaceBackgroundColor = grayColor
}
