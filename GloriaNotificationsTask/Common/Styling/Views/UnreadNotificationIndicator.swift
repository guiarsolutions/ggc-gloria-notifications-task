
import UIKit

@IBDesignable public class UnreadNotificationIndicator: UIView {
        
    @IBInspectable public var radius: CGFloat = 4.0 {
        didSet {
            setupLayer()
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupLayer()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        setupLayer()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        setupLayer()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    public override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        setupLayer()
    }
    
    private func setupLayer() {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
    
    private func setup() {
        backgroundColor = Colors.unreadNotificationIndicatorColor
    }
}
