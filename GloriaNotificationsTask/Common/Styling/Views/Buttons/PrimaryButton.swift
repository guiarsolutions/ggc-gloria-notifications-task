
import UIKit

@IBDesignable class PrimaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        style()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        style()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        style()
    }
    
    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        style()
    }
    
    private func style() {
        tintColor = Colors.primaryButtonTitleColor
        backgroundColor = Colors.primaryButtonBackgroundColor
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
}
