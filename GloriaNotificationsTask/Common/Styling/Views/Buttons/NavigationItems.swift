
import UIKit

class NavigationItems {
    
    static func close(target: Any, action: Selector?) -> UIBarButtonItem {
        
        return UIBarButtonItem(image: UIImage(named: "navigation-close-x"),
                               style: .plain,
                               target: target,
                               action: action)
    }
}
